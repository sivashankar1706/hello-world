package hello;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
interface Addable{  
    int add(int a,int b);  
}  

public class Hello implements Addable{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> nameList = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
		
		List<Integer> updatedList = nameList.stream().filter(element -> element%2==0).collect(Collectors.toList());
		updatedList.stream().forEach(System.out::println);
		
		//System.out.println("updatedList : " + updatedList);
		int sum = 0;
		for(int a : updatedList) {
		//	System.out.println("a : " + a);
			sum+=a;
		}
		//System.out.println("sum : " + sum);
		
		Runnable runn = () -> System.out.println("in run...");
		runn.run();
		
		Addable ad = (a, b) -> (a+b);
		//System.out.println(ad.add(5, 6));
		Addable ad1 = (c,d) -> (c+d);
		ad1.add(10, 20);
		
	}

	@Override
	public int add(int a, int b) {
		// TODO Auto-generated method stub
		return a+b;
	}

}
