package com.fetchdata.fetchExample;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FetchDataService extends CrudRepository<UserModel, Integer>{

	@Override
	List<UserModel> findAll();
}
