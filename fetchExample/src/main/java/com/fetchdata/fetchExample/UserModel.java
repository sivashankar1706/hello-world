package com.fetchdata.fetchExample;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name = "employee_details")
public class UserModel {
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EmployeeName")
	String EmployeeName;
	
	@Column(name = "Age")
	Integer Age;
	
	@Column(name = "DateofBirth")
	String DateofBirth;
	
	@Column(name = "ProjectName")
	String ProjectName;
	
	@Column(name = "EmployerName")
	String EmployerName;
	
	@Column(name = "Salary")
	Integer Salary;
	
	@Column(name = "YearsofExperience")
	Integer YearsofExperience;
	public Integer getAge() {
		return Age;
	}public String getDateofBirth() {
		return DateofBirth;
	}public String getEmployeeName() {
		return EmployeeName;
	}public String getEmployerName() {
		return EmployerName;
	}public String getProjectName() {
		return ProjectName;
	}public Integer getSalary() {
		return Salary;
	}public Integer getYearsofExperience() {
		return YearsofExperience;
	}public void setAge(Integer age) {
		Age = age;
	}public void setDateofBirth(String dateofBirth) {
		DateofBirth = dateofBirth;
	}public void setEmployeeName(String employeeName) {
		EmployeeName = employeeName;
	}public void setEmployerName(String employerName) {
		EmployerName = employerName;
	}public void setProjectName(String projectName) {
		ProjectName = projectName;
	}public void setSalary(Integer salary) {
		Salary = salary;
	}public void setYearsofExperience(Integer yearsofExperience) {
		YearsofExperience = yearsofExperience;
	}
}
