package com.fetchdata.fetchExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FetchExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FetchExampleApplication.class, args);
	}

}
