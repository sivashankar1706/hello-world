package com.finalreg;


import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.finalreg.repo.UserRepo;

@Controller
public class RegistrationController {

	@Autowired
	UserRepo repo;
	
	@RequestMapping("/regform")
	public String register() {
		return "registration";
	}
			
	@RequestMapping("/setuser")
	public String userInfo(UserRegistration reg) {
		repo.save(reg);
		return "login";
	}
	
	@RequestMapping("/") 
	 public String operation() {
		 return "login";
	 }
	
	@RequestMapping("/login")
	public String loginHomePage(@RequestParam ("user_name") String user_name,
								@RequestParam ("password") String password,Model model)
	{
		Iterable<UserRegistration> u = null;
		u = repo.findAll();
		Iterator<UserRegistration> iterator = u.iterator();
		while(iterator.hasNext()) {
			UserRegistration user = iterator.next();
			if(user.getUser_name().equals(user_name) && user.getPassword().equals(password)) {
				model.addAttribute("user_name", user_name);
				return "homePage";
			}
		}
		model.addAttribute("error", "User Name or Password is wrong!!! Do Register!!!");
		return "login";
	}
	@RequestMapping("/homePage")
	public String goToHomePage() {
		return "homePage";
	}
	@RequestMapping("/changePwd")
	public String change() {
		return "resetLink";
	}
	
	@RequestMapping(value="/reset", method = RequestMethod.POST)
	public String resetPwd(@RequestParam ("mail_id") String mail_id,
								@RequestParam ("password") String password,Model model)
	{
		//System.out.println("hello");
		Iterable<UserRegistration> u =repo.findAll();
		Iterator<UserRegistration> iterator = u.iterator();
		while(iterator.hasNext()) {
			UserRegistration user = iterator.next();
			if(user.getMail_id().equals(mail_id)) {
				user.setPassword(password);
				repo.save(user);
				//model.addAttribute("error","Now Login with new Password!!");
				return "login";
			}
	}
		model.addAttribute("error", "Email id is wrong!!! Click here to reset!!!");
		return "login";
	}
}	
	/*
	 * @RequestMapping("/reset") public String goToChangePassword(UserRegistration
	 * change) { repo.save(change); return "login"; }
	 */

	/*@RequestMapping("/userdetail")
	public ModelAndView userInfo(UserRegistration reg) {
		//System.out.println("I am called....."+reg.getUser_name());
		ModelMap model = new ModelMap();
		model.addAttribute("name", reg.getName());
		model.addAttribute("date_of_birth", reg.getDate_of_birth());
		model.addAttribute("mail_id", reg.getMail_id());
		model.addAttribute("address", reg.getAddress());
		model.addAttribute("user_name", reg.getUser_name());
		model.addAttribute("password", reg.getPassword());
		
		ModelAndView mv = new ModelAndView("user-details");
		mv.addObject("regObj", model);
		return mv;*/


