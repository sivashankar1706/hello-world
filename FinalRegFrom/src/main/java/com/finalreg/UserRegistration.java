package com.finalreg;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reg")
public class UserRegistration {

	@Id
	private long emp_no;
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	//private Long id;
	private String name;
	private int age;
	private String date_of_birth;
	private String mail_id;
	private String address;
	private String user_name;
	private String password;
	
	public long getEmp_no() {
		return emp_no;
	}
	public void setEmp_no(long emp_no) {
		this.emp_no = emp_no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public String getMail_id() {
		return mail_id;
	}
	public void setMail_id(String mail_id) {
		this.mail_id = mail_id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

	@Override
	public String toString() {
		return "UserRegistration [emp_no=" + emp_no + ", name=" + name + ", age=" + age + ", date_of_birth="
				+ date_of_birth + ", mail_id=" + mail_id + ", address=" + address + ", user_name=" + user_name
				+ ", password=" + password + "]";
	}
}
