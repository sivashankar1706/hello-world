<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success</title>
</head>
<body>
<h1 align="center">Data Entered Successfully in Database!!!!!!!</h1><br><br>
<h1 align="center">LOGIN PAGE</h1><br>
<center>
<form action="/login">
			User Name : 		<input type = "text" id="user_name" name="user_name" required><br><br>
            Password : 			<input type = "password" id="password" name="password" required><br><br>
        	<c:if test="${not empty error}">
			<div><h4>${error}<input type="button" value="Change Password" onclick="goToChangePwd()"></h4></div>
			<div><br></div>
			</c:if>
        	<input type= "submit" value="Login">
        	<input type = "submit" value = "Register" onclick="goToRegister()">
</form></center>
<script type="text/javascript">
function goToChangePwd(){
	window.location.href="/changePwd";
}
function goToRegister(){
	window.location.href="/regform";
	}
</script>			
</body>
</html>