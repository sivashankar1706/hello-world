package com.regform1;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*import lombok.Data;
import lombok.Value;

@Data*/
@Entity
@Table(name = "registration")
public class RegModel {
	@Id
	@Column(name = "emp_no")
	private String emp_no;
	
	@Column(name= "name")
	private String name;
	
	@Column(name= "age")
	private int age;
	
	@Column(name= "date_of_birth")
	private String date_of_birth;
	
	@Column(name = "mail_id")
	private String mail_id;
	
	@Column(name = "address")
	private String address;
	
	@Column(name= "user_name")
	private String user_name;
	
	@Column(name= "password")
	private String password;
	
}
