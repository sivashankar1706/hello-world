package com.regform1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegController {
	
	@Autowired
	RegRepository regRepository;
	
	@CrossOrigin(origins = "http://localhost:8080")
	@GetMapping("/employee")
	public @ResponseBody Iterable<RegModel> getData()
	{
		//System.out.print(employeeRepository);
		return regRepository.findAll();
	}
	
	@PostMapping("/addemp")
	public RegModel addEmployee(@RequestBody RegModel emp)
	{
		System.out.print("Comming");
		return  regRepository.save(emp);
	}
	
	/*@PostMapping("/updateEmployee")
	public EmployeeModel updateEmployee(@RequestBody EmployeeModel emp)
	{
		employeeRepository.delete(emp);
		return employeeRepository.save(emp);
	}*/
	@GetMapping("/test")
	public String Test()
	{
		return "Working";
	}
}