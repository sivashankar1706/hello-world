package com.regform1;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegRepository extends CrudRepository<RegModel,Integer> {

}

